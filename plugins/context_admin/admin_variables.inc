<?php

/**
 * Plugins are described by creating a $plugin array which will be used by the
 * system that includes this file.
 */
$plugin = array(
  'title' => t('Variable setting page'),
  'description' => t('Variable setting pages are used to let site administrators
    set selected Drupal variables. They can in best case replace custom-written
    pages for variable settings.'),
  'content form' => 'context_admin_admin_variables_content_form',
  'content form submit' => 'context_admin_admin_variables_content_form_submit',
  'render' => 'context_admin_admin_variables_render_page',
);

/**
 * The configuration form in Page manager for the plugin admin_variables.
 */
function context_admin_admin_variables_content_form($form, $form_state, $cache = NULL) {
  // Get default values.
  if (!is_null($form_state['handler_id'])) {
    $default_options =
      isset($form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_admin_variables'])
        ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_admin_variables']
        : isset($cache->handlers[$form_state['handler_id']]->conf['context_admin_admin_variables'])
        ? $cache->handlers[$form_state['handler_id']]->conf['context_admin_admin_variables']
        : NULL;
  }
  else {
    $default_options =
      $form_state['page']->new_handler->conf['context_admin_admin_variables'];
  }

  // Build the configuration form.
  $form['variables'] = array(
    '#type' => 'textarea',
    '#default_value' => $default_options,
    '#title' => t('Variable names'),
    '#description' => t('Enter one variable name per line. All names will be
      plain_checked, and if they don\'t already exist in the variables table
      they will be created. Best practice dictates that all your variable names
      should start with your module name.'),
  );
  return $form;
}

/**
 * Submit function for the plugin admin_variables.
 */
function context_admin_admin_variables_content_form_submit($form, &$form_state) {
  // Store the settings in the handler configuration.
  if (isset($form_state['handler_id'])) {
    $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_admin_variables']
      = $form_state['values']['variables'];
  }
  else {
    $form_state['page']->new_handler->conf['context_admin_admin_variables']
      = $form_state['values']['context_admin_admin_variables'];
  }
  context_admin_set_page_cache($form_state['page']->subtask_id, $form_state['page']);
}

/**
 * Page callback for the end-user (admin) page for the plugin admin_variables.
 */
function context_admin_admin_variables_render_page($handler, $base_contexts, $args, $test = TRUE) {
  // Get the variable names.
  $variables = explode("\r", $handler->conf['context_admin_admin_variables']);
  foreach ($variables as $key => $variable) {
    $variables[$key] = trim(check_plain($variable));
  }

  // Build the form
  $form['variables'] = array(
    '#type' => 'fieldset',
    '#title' => 'Variables',
    '#tree' => FALSE,
  );
  foreach ($variables as $variable) {
    $form['variables'][$variable] = array(
      '#type' => 'textfield',
      '#title' => $variable,
      '#default_value' => variable_get($variable, NULL),
      '#tree' => FALSE,
    );
  }

  // Add some formatting to the form, and return it.
  return drupal_get_form('context_admin_admin_variables_form', &$form);
}

/**
 * Helper function for caching the form to display for end users (admins).
 *
 * Acutally, this function is called because I couldn't get the freakin'
 * system_settings_form() to work in the regular page callback. But this also
 * has the benefit of the form being cached. Blame @Itangalo.
 */
function context_admin_admin_variables_form($unknown_1, $unknown_2, &$form) {
  return system_settings_form($form);
}
